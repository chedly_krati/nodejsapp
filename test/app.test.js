const request = require('supertest');
const express = require('express');
const app = require('../app'); // Adjust the path if necessary

describe('GET /', () => {
  it('responds with This is my Node application for CICD!!', (done) => {
    request(app)
      .get('/')
      .expect('Content-Type', /text/)
      .expect(200, 'This is my Node application for CICD!!', done);
  });
});
